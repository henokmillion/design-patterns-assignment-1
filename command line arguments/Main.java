public class Main {
    public static void main(String[] args) {
        if (args.length < 2) {
            System.out.println("no args? well, what can you do...");
        } else {
            for (String arg: args) {
                System.out.println("<arg>: " + arg);
            }
        }
    }
}