// import DbDelegate;

public class Client {
    DbDelegate dbService;

    public Client(DbDelegate dbService) {
        this.dbService = dbService;
    }

    public void doTask() {
        dbService.doTask();
    }
}