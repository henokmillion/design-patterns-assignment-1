// import MongoDbService;
// import OracleDbService;

public class DbLookup {
    public DbService getDbService(String serviceType) {
        if (serviceType == "Oracle") {
            return new OracleDbService();
        } else {
            return new MongoDbService();
        }
    }
}