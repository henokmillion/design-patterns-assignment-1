using System;

namespace ConsoleApplication {
    public class Program {
        public static void Main(string[] args) {
            English english = new Kenyan();
            english.SaySomething();
        }
    }

    public class English {
        public virtual void SaySomething() {
            System.Console.WriteLine("Hello");
        }
    }

    public class Kenyan: English {
        public Kenyan() {
        }

        public override void SaySomething() {
            System.Console.WriteLine("Halo");   
			base.SaySomething();
        }
    }
}