package com.company;

import com.company.Builder.BichaFordBuilder;
import com.company.Builder.Car;
import com.company.Builder.CarBuilder;

import java.util.HashMap;

public class Main {

    public static void main(String[] args) {
	// write your code here
//        HashMap<String, Shape> shapes = new HashMap<String, Shape>();
//        shapes.put("rectangle", Rectangle.getInstance());
//
//        ShapeFactory factory = new ShapeFactory();
//        factory.loadShapes(shapes);
//        Rectangle rectangle = (Rectangle) factory.getShape("rectangle");
//        rectangle.draw();
        Car bichafordBuilder = CarBuilder.getBuilder("bicha ford").build();
        System.out.println(bichafordBuilder.getWheels());
        System.out.println(bichafordBuilder.getColor());

    }
}
