package com.company;

public class Rectangle extends Shape {
    @Override
    public void draw() {
        System.out.println("drawing a rectangle");
    }

    private Rectangle() {
        super();
    }

    static Rectangle instance = new Rectangle();

    public static Shape getInstance() {
        return instance;
    }


}
