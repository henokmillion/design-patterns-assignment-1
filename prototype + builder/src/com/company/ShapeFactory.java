package com.company;

import java.util.ArrayList;
import java.util.HashMap;

public class ShapeFactory extends Factory {

    private HashMap<String, Shape> shapes = new HashMap<String, Shape>();

    @Override
    public Shape getShape(String shape) {
        return (Shape) shapes.get(shape).clone();
    }

    @Override
    public void loadShapes(HashMap<String, Shape> shapes) {
        for (String shape: shapes.keySet()) {
            this.shapes.put(shape, shapes.get(shape));
        }
    }
}
