package com.company.Builder;

public class BichaFordBuilder {
    BichaFord bichaFord = BichaFord.getInstance();
    String color;
    int wheels;

    private BichaFordBuilder() {
    }
    private static BichaFordBuilder instance = new BichaFordBuilder();
    public static BichaFordBuilder getInstance() {
        return instance;
    }

    public void setColor() {
        bichaFord.setColor("Bicha");
    }

    public void fitWheels() {
        bichaFord.fitWheels(3);
    }

    public BichaFord build() {

        this.setColor();
        this.fitWheels();
        return bichaFord;
    }
}
