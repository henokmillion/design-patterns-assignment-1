package com.company.Builder;

public abstract class Car {
    public abstract void fitWheels(int wheels);

    public abstract String getColor();

    public abstract void setColor(String color);

    public abstract int getWheels();
}
