package com.company.Builder;

public class BichaFord extends Car {
    private BichaFord() {}
    private static BichaFord instance = new BichaFord();
    public static BichaFord getInstance() {
        return instance;
    }

    int wheels;
    String color;

    @Override
    public void fitWheels(int wheels) {
        this.wheels = wheels;
    }

    @Override
    public String getColor() {
        return color;
    }

    @Override
    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public int getWheels() {
        return this.wheels;
    }
}
