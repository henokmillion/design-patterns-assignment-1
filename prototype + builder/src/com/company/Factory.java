package com.company;

import java.util.HashMap;

public abstract class Factory {
    public abstract Shape getShape(String shape);
    public abstract void loadShapes(HashMap<String, Shape> shape);
}
