
public class Main {
    public static void main(String[] args) {
        DbDelegate dbDelegate = new DbDelegate();
        dbDelegate.setDbServiceType("Mongodb");

        Client client = new Client(dbDelegate);
        client.doTask();

        dbDelegate.setDbServiceType("Oracle");
        client.doTask();
    }
}