// import DbService;
// import DbLookup;
public class DbDelegate {
    private DbLookup dbLookupService = new DbLookup();
    private DbService dbService;
    private String dbServiceType;

    public void setDbServiceType(String serviceType) {
        dbServiceType = serviceType;
    }

    public void doTask() {
        dbService = dbLookupService.getDbService(dbServiceType);
        dbService.fetchData();
    }
}