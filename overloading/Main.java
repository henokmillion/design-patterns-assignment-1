import java.lang.Math.*;

public class Main {
    public static void main(String[] args) {
        if (args.length >= 2) {
            register(args[0], args[1]);
        } else if (args.length == 1) {
            register(args[0]);            
        } else {
            System.out.println("no arguments? well, what can you do...");            
        }
    }

    public static void register(String name) {
        System.out.println("registered as:: name:" + name + ", id: 0000");
    }

    public static void register(String name, String id) {
        System.out.println("registered as:: name:" + name + ", id: " + id);
    }
}