abstract class Animal  {
    public abstract makeSound(): void;
}

class Dog extends Animal {
    public makeSound(): void {
        console.log('wuf! wuf!');
    }
}

class Cat extends Animal {
    public makeSound(): void {
        console.log('meow!');
    }
}


let animals: Animal[] = [new Cat(), new Dog()];

for (let animal of animals) {
    animal.makeSound();
}